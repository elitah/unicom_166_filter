package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"strings"
	"time"
)

type ConfigStruct struct {
	ProvinceShowHuiTag string   `json:"provinceShowHuiTag"`
	SplitLen           string   `json:"splitLen"`
	NumRetailList      []string `json:"numRetailList"`
	Code               string   `json:"code"`
	Uuid               string   `json:"uuid"`
	NumArray           []uint64 `json:"numArray"`
}

func getNumsArray(url string) string {
	c := http.Client{
		Transport: &http.Transport{
			Dial: func(netw, addr string) (net.Conn, error) {
				deadline := time.Now().Add(3 * time.Second)
				c, err := net.DialTimeout(netw, addr, 3*time.Second)
				if err != nil {
					return nil, err
				}
				c.SetDeadline(deadline)
				return c, nil
			},
		},
	}
	if resp, err := c.Get(url); nil == err {
		defer resp.Body.Close()
		if body, err := ioutil.ReadAll(resp.Body); nil == err {
			return string(body[:])
		}
	}
	return ""
}

func worker(id int, results chan<- float64) {
	for {
		jsonp := getNumsArray("https://m.10010.com/NumApp/NumberCenter/qryNum?provinceCode=51&cityCode=510&monthFeeLimit=0&groupKey=21236872&searchCategory=3&net=01&amounts=200&codeTypeCode=&searchValue=&qryType=02")
		if 0 < len(jsonp) {
			x := strings.IndexAny(jsonp, "{")
			y := strings.LastIndex(jsonp, "}")
			if 0 <= x && x < y {
				jsonstr := string(jsonp[x : y+1])
				var config ConfigStruct
				if err := json.Unmarshal([]byte(jsonstr), &config); nil == err {
					for _, num := range config.NumArray {
						if 10000000000 < num {
							n1 := num / 100000000
							n2 := num % 100000000 / 10000
							n3 := num % 10000
							if 166 == n1 {
								if 2015 <= n2 && 2020 >= n2 {
									x1 := n3 / 1000
									x2 := n3 % 1000 / 100
									x3 := n3 % 100 / 10
									x4 := n3 % 10
									if 4 != x1 && 4 != x2 && 4 != x3 && 4 != x4 {
										results <- float64(num)
									}
								}
							}
						}
					}
				}
			}
		}
	}

	fmt.Printf("worker[%d]: processing job done exit\n", id)
}

func main() {
	list := make(map[float64]byte)

	results := make(chan float64, 100)

	for i := 0; 20 > i; i++ {
		go worker(i, results)
	}

	for 100 >= len(list) {
		list[<-results] = 0

		fmt.Println(len(list))
	}

	for k, _ := range list {
		fmt.Println(uint64(k))
	}
}
