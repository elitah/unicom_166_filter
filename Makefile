
main::
	GOPATH=$(PWD) go fmt main
	GOPATH=$(PWD) go build main

clean:
	rm -rf main

distclean:
	make -C . clean
